#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Rentrer une lettre : ");
    char c; 
    c = getchar();

    printf("Votre lettre est : ");
    putchar(c); 

    printf("\n");

    return 0;
}
