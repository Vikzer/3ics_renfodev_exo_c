#include<stdio.h>

int main(int argc, char const *argv[])
{

    double x1=1.2345;
    double x2=123.45;
    double x3=0.000012345;
    double x4=1e-10;
    double x5=-123.4568e15;

    printf("Nombre réel au format decimal : \n x1 = %f \n x2 = %f \n x3 = %f \n x4 = %f\n x5 = %f \n \n",x1,x2,x3,x4,x5);
    printf("Nombre réel décimal en notation scientifique : \n x1 = %e \n x2 = %e \n x3 = %e \n x4 = %e\n x5 = %e \n",x1,x2,x3,x4,x5);

    return 0;
}
